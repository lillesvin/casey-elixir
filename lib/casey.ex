defmodule Casey do
  def get_xml(url) do
    HTTPoison.start
    case HTTPoison.get(url) do
      {:ok, %HTTPoison.Response{status_code: 200, body: body}} ->
        {:ok, body}
      _ ->
        {:error, "Error fetching XML."}
    end
  end

  def parse_xml(xml) do
    import SweetXml

    items = xml |> xpath(
      ~x"//item"l,
      title: ~x"./title/text()"s,
      link: ~x"./link/text()"s,
      category: ~x"./category/text()"s,
      pub_date: ~x"./pubDate/text()"s
    )
    {:ok, items}
  end

  def get_list() do
    {:ok, xml} = Application.get_env(:casey, :filter_url) |> get_xml
    parse_xml(xml)
  end

  def print_list(list) do
    list |> Enum.map(fn(item) ->
      IO.puts " - #{item.title} (#{item.link})"
    end)
  end

  def build_slack_message(list) do
    attachments = list |> Enum.map(fn(item) ->
      %SlackMessage.Attachment{
        fallback: "#{item.title} <#{item.link}>",
        text: "<#{item.link}|#{item.title}>",
        color: "#3333AA",
        fields: [
          %SlackMessage.Attachment.Field{
            title: "Category",
            short: true,
            value: "#{item.category}"
          },
          %SlackMessage.Attachment.Field{
            title: "Opened",
            short: true,
            value: "#{item.pub_date}"
          }
        ]
      }
    end)
    %SlackMessage{
      channel: Application.get_env(:casey, :channel),
      username: Application.get_env(:casey, :username),
      text: Application.get_env(:casey, :intro_text),
      attachments: attachments
    }
  end
end

defmodule CaseyJones do
  def print do
    {:ok, list} = Casey.get_list
    Casey.print_list(list)
  end

  def slack_it do
    {:ok, list} = Casey.get_list
    msg = list |> Casey.build_slack_message
    case Slack.send(msg) do
      {:error, error} -> IO.puts "Error while sending to Slack: #{error}"
      {:ok, _} -> true
    end
  end
end
