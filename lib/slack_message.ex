defmodule SlackMessage do
    @derive [Poison.Encoder]
    defstruct [:channel, :username, :text, :icon_emoji, :attachments]
    @type t :: %__MODULE__{channel: string, username: string, text: string, icon_emoji: string, attachments: list}
end

defmodule SlackMessage.Attachment do
    @derive [Poison.Encoder]
    defstruct [:fallback, :text, :pretext, :color, :fields]
    @type t :: %__MODULE__{fallback: string, text: string, pretext: string, color: string, fields: list}
end

defmodule SlackMessage.Attachment.Field do
    @derive [Poison.Encoder]
    defstruct [:title, :value, :short]
    @type t :: %__MODULE__{title: string, value: string, short: boolean}
end

defmodule Slack do
    def send(message) do
        webhook_url = Application.get_env(:casey, :webhook_url)
        HTTPoison.post(webhook_url, Poison.encode!(message))
    end
end