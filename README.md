# Casey Jones (Elixir)

Small tool that posts cases from a FogBugz filter in Slack. This is the Elixir version.

Edit config/config.exs and call `CaseyJones.slack_it()` from e.g. `iex -S mix`.
