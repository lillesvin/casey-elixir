# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
use Mix.Config

config :casey, :filter_url, "https://example.fogbugz.com/default.asp?pg=pgRss&..."
config :casey, :webhook_url, "https://hooks.slack.com/services/*/*"
config :casey, :channel, "#general"
config :casey, :username, "Casey Jones"
config :casey, :intro_text, "Found the following in your FogBugz filter..."

